package com.hanifalbaaits.java.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.hanifalbaaits.java.service.PegawaiService;
import com.hanifalbaaits.java.split.SplitUtils;

@Component
@Service
public class CoreSoyal implements Runnable {
		
	private static Logger logger = LoggerFactory.getLogger(CoreSoyal.class);
	
	String username_cfg;
	String password_cfg;
	String ip_cfg;
	
	public void setUsername(String user){
		this.username_cfg = user;
	}
	public void setPassword(String pass){
		this.password_cfg = pass;
	}
	public void setIp(String ip){
		this.ip_cfg = ip;
	}
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	PegawaiService pegawaiService;
	
	@Autowired
	SplitUtils split;
	
	@Value("${jms.soyal.absen}")
	String JMS_QUEUE;
	
	@Value("${soyal.core.username}")
	String usernameC;
	
	@Value("${soyal.core.password}")
	String passwordC;
	
	@Value("${soyal.core.ipcontroller}")
	String ipSource;
		
	String logSource = "EventLog.htm";

	private Date dtDummydt;
	
	@Override
	public void run() {
		try {	
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
			String dummy = "0000-00-00 00:00:00";
			this.dtDummydt = format.parse(dummy);
			logger.info("mulai, dtDummydt: "+this.dtDummydt);
            for (;;) {
                try {
                	//logger.info("startServer running");
                    startServer();
                } catch (Exception e) {
                    logger.debug("Exception xxx : " + e.getMessage());
                }
                Thread.sleep(300); //jeda untuk pengambilan data;
            }
        } catch (Exception e) {
            logger.debug("Exception run : " + e.getMessage());
        }
	}
	
	private void startServer() throws Exception,ParseException {
		//logger.debug("configure: "+this.username_cfg+"&"+this.password_cfg+"&"+this.ip_cfg);
		//String login = usernameC+":"+passwordC;
		String login1 = this.username_cfg+":"+this.password_cfg;
		//logger.debug("dtdummy tgl: "+this.dtDummydt);
		//logger.info("configure1: "+login);
		//logger.debug("configure2: "+login1);
		String base64login = new String(Base64Utils.encode(login1.getBytes()));
		
	    try {
			 	String url = "http://"+this.ip_cfg+"/"+logSource;
	            //logger.info(url);
			 	Document doc = Jsoup.connect(url)
	                    .header("Authorization", "Basic " + base64login)
	                    .get();
	            
	            Element tables = doc.select("table").get(1); //select the first table.
	            Elements rows = tables.select("tr");
	                            
	            for (int i = 0; i < rows.size(); i++) { //first row is the col names so skip it.
	                Element row = rows.get(i);
	                Elements tds = row.select("td");
	                if (tds.size() > 6) {
	                	 if (!tds.get(0).text().equalsIgnoreCase("Index")) {
	                		
	                         Date date = split.SplitTanggalTime(tds.get(1), tds.get(2)); //parameter date dan time
	                         
	                		 if(date.compareTo(this.dtDummydt) > 0) {
	                			System.out.println("============================"); 
	                			System.out.println("dtDummy	: "+this.dtDummydt); 
	                			System.out.println("dtNow	: "+date); 
	                			System.out.println("tanggal / time lebih besar dari dummy. data baru"); 
	                			logger.info("IP Controller: "+this.ip_cfg);
	                		 	logger.info("Tanggal: "+tds.get(1));
	                		 	logger.info("Time: "+tds.get(2));
	                		 	logger.info("Address: "+tds.get(3));
	                		 	logger.info("Display: "+tds.get(4));
	                		 	logger.info("Access Detail: "+tds.get(5));           		 	
	                		 	logger.info("Card UID: "+tds.get(6));
	                		 	logger.info("Door: "+tds.get(7));
	                		 	
	                		 	String index = split.SplitTD(tds.get(0));
	                		 	String dte = split.SplitTD(tds.get(1));
	                		 	dte = dte.replace("'", "/");
	                		 	String time = split.SplitTD(tds.get(2));
	                		 	String uid = split.SplitTD(tds.get(3)).trim();
	                		 	String display = split.SplitTD(tds.get(4));
	                		 	String detail = split.SplitTD(tds.get(5));
	                		 	String card_uid = split.SplitTD(tds.get(6)).trim();
	                		 	String door = split.SplitTD(tds.get(7));
	                		 	logger.info(detail+"  "+uid+"  "+card_uid);
	                		 	
	                		 	SimpleDateFormat formatRio = new SimpleDateFormat("yyyy-MM-dd"); 
	                		 	String tanggal = formatRio.format(date);
	                            
	                		 	jmsTemplate.convertAndSend("core-soyal-listener", index+"&"+dte+"&"+time+"&"+uid+"&"+display+"&"+
	                		 			detail+"&"+card_uid+"&"+door+"&"+tanggal+"&"+this.ip_cfg);
	                		 	
	                		 	this.dtDummydt= date;
	                		 	} 
	                	 }
	                }
	            }
	     } catch (Exception e) {
             e.printStackTrace();
             logger.debug("Exception core : " + e.getMessage());
      }
	}
}
