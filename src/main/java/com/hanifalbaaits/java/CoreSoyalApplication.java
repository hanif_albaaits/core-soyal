package com.hanifalbaaits.java;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jms.annotation.EnableJms;
//import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

//import com.hanifalbaaits.java.config.AppConfig;
import com.hanifalbaaits.java.core.CoreSoyal;
import com.hanifalbaaits.java.model.DataController;
import com.hanifalbaaits.java.service.DataControllerService;

@SpringBootApplication
@EnableJms
@Component
public class CoreSoyalApplication implements CommandLineRunner {
	
	private static Logger logger = LoggerFactory.getLogger(CoreSoyalApplication.class);
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	CoreSoyal coreSoyal;
	
	@Autowired
	DataControllerService controllerService;
	
	public static void main(String[] args) {
		SpringApplication.run(CoreSoyalApplication.class, args);		
	}
	
	public void run(String... args) throws Exception {
		
		//ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		//ThreadPoolTaskExecutor taskExecutor = (ThreadPoolTaskExecutor) context.getBean("taskExecutor");
		
		List<DataController> controllerActive = controllerService.findControllerActive();
		if (controllerActive.size()>0) {
			for (DataController controller : controllerActive) {
				logger.info("controller active IP: "+controller.getIp());
				logger.info("username : "+controller.getUser());
				logger.info("Password : "+controller.getPassword());
				
				CoreSoyal control = new CoreSoyal();
				applicationContext.getAutowireCapableBeanFactory().autowireBean(control);
				//control = (CoreSoyal) context.getBean("CoreSoyal");
				control.setIp(controller.getIp());
				control.setUsername(controller.getUser());
				control.setPassword(controller.getPassword());	
				//taskExecutor.execute(control);
				new Thread(control).start();		
			}
			
		} else {
			logger.info("Nothing contoller active");
		}
		
		//new Thread(coreSoyal).start();
	}
}
