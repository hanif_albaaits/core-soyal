package com.hanifalbaaits.java.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.hanifalbaaits.java.model.DataController;

@Repository
public interface DataControllerRepo extends JpaRepository<DataController, Integer> {

	@Query("select v from DataController v where v.status=1")
	List<DataController> findControllerActive();
	
//	@Query(nativeQuery=true, 
//			value="select * from tb_card where status=1")
//	List<DataController> findControllerActive();
}
