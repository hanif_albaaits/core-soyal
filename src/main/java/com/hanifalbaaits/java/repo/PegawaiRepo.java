package com.hanifalbaaits.java.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.model.Pegawai;

@Repository
public interface PegawaiRepo extends JpaRepository<Pegawai, Integer>{
	
	@Query("select v from Pegawai v")
	List<Pegawai> findAll();
	
	@Query("select v from Pegawai v where v.status=1")
	List<Pegawai> findAllActive();
	
	@Query("select v from Pegawai v where v.status=1 and v.id_div=?1")
	List<Pegawai> findAllActiveByDiv(int div);
	
	@Query("select v from Pegawai v where v.status=1 and v.address_uid like concat('%',?1,'%')")
	Pegawai findActiveByAddress(String addess);
	
	@Query("select v from Pegawai v where v.status=1 and v.uid like concat('%',?1,'%')")
	Pegawai findActiveByUID(String uid);
}
