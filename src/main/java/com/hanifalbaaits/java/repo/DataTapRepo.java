package com.hanifalbaaits.java.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.hanifalbaaits.java.model.DataTap;

@Repository
public interface DataTapRepo extends JpaRepository<DataTap, Integer>{
	
	@Query("select v from DataTap v where v.id=?1")
	DataTap findById(int id);
}
