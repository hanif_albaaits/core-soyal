package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.model.DataController;
import com.hanifalbaaits.java.repo.DataControllerRepo;

@Service
public class DataControllerService {
	
	@Autowired
	DataControllerRepo controllerRepo;
	
	public List<DataController> findControllerActive(){
		return controllerRepo.findControllerActive();
	}
	
	public void save(DataController data) {
		this.controllerRepo.save(data);
	}
	
	public void delete(DataController data) {
		this.controllerRepo.delete(data);
	}
}
