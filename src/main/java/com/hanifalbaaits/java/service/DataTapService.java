package com.hanifalbaaits.java.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.model.DataTap;
import com.hanifalbaaits.java.repo.DataTapRepo;

@Service
public class DataTapService {
	
	@Autowired
	DataTapRepo tapRepo;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public DataTap findAgentsById(int id) {
		return tapRepo.findById(id);
	}
	
	public List<DataTap> findQueue(int limit) {
		return entityManager.createQuery("SELECT p FROM DataTap p ORDER BY p.id DESC",
		          DataTap.class).setMaxResults(limit).getResultList();
	}
	
	public void save(DataTap data) {
		this.tapRepo.save(data);
	}
	
	public void delete(DataTap data) {
		this.tapRepo.delete(data);
	}
}
