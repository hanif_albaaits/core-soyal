package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hanifalbaaits.java.model.Pegawai;
import com.hanifalbaaits.java.repo.PegawaiRepo;

@Service
public class PegawaiService {
	
	@Autowired
	PegawaiRepo pegawaiRepo;
	
	public List<Pegawai> findAll(){
		return pegawaiRepo.findAll();
	}
	
	public List<Pegawai> findAllActive(){
		return pegawaiRepo.findAllActive();
	}
	
	public List<Pegawai> findAllActiveByDiv(int div){
		return pegawaiRepo.findAllActiveByDiv(div);
	}
	
	public Pegawai findActiveByAddress(String address){
		return pegawaiRepo.findActiveByAddress(address);
	}
	
	public Pegawai findActiveByUID(String UID){
		return pegawaiRepo.findActiveByUID(UID);
	}
	
	public void save(Pegawai data) {
		this.pegawaiRepo.save(data);
	}
	
	public void delete(Pegawai data) {
		this.pegawaiRepo.delete(data);
	}
}
