package com.hanifalbaaits.java.split;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;

@Component
public class SplitUtils {
	
	public Date SplitTanggalTime(Element tgl, Element tm) throws ParseException {
		//System.out.println(tgl);
		//System.out.println(tm);
		String tanggal = SplitTD(tgl);
		String time = SplitTD(tm);
		//System.out.println(tanggal);
		//System.out.println(time);
		
		//tanggal = "19'07/12";
		//time = "07:40:56";
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		String tahun = "20"+tanggal.split("\\'")[0];
		tanggal = tanggal.replace("'", "/");
		tanggal = tahun+"-"+tanggal.split("\\/")[1]+"-"+tanggal.split("\\/")[2]+" "+time;
		Date date=format.parse(tanggal);		
		//System.out.println(tanggal);
		//System.out.println("date: "+date);
		return date;
	}
	
	public String SplitTD(Element td) {
		String data1 = td.toString();
		String sub = data1.substring(4, data1.length()-5);
		return sub;
	}
}
