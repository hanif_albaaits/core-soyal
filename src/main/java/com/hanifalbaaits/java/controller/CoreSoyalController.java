package com.hanifalbaaits.java.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.model.DataController;
import com.hanifalbaaits.java.model.ResponAPI;
import com.hanifalbaaits.java.service.DataControllerService;

@RestController
@RequestMapping(value="/api/coresoyal")
public class CoreSoyalController {
	
	private static Logger logger = LoggerFactory.getLogger(CoreSoyalController.class);
	
	@Autowired
	ResponAPI responAPI;
	
	@Autowired
	DataControllerService controllerService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/changecard")
	public ResponseEntity<String> changeCard(@RequestBody Map<String, String> body) throws Exception,ParseException {
		
		logger.info("==== Hit Api ChangeCard ====");
		if (body.toString()==null) return new ResponseEntity(responAPI.BAD_REQUEST_400("Body Null"),HttpStatus.BAD_REQUEST);
		
		logger.info("Address Card : "+ body.get("address"));
		if (body.get("address")==null || body.get("address").toString().isEmpty()) return new ResponseEntity(responAPI.BAD_REQUEST_400("Field Address Null"),HttpStatus.BAD_REQUEST);
		logger.info("UID Card : "+ body.get("uid"));
		if (body.get("uid")==null || body.get("uid").toString().isEmpty()) return new ResponseEntity(responAPI.BAD_REQUEST_400("Field UID Null"),HttpStatus.BAD_REQUEST);
		logger.info("status : "+ body.get("status"));
		if (body.get("status")==null || body.get("status").toString().isEmpty()) return new ResponseEntity(responAPI.BAD_REQUEST_400("Field UID Null"),HttpStatus.BAD_REQUEST);

		String address = body.get("address").toString().trim();
		String uid = body.get("uid").toString().trim();
		String stat = null;
		if(body.get("status").toString().contentEquals("1")) {
			stat = "Card Only";
		} else {
			stat = "Invalid Card";
			uid = "65535:65535"; //waktu memblokir ganti UID yang tidak dikenal .
		}
		logger.info("change status :"+stat+"  uid:"+uid);
		List<DataController> controllerActive = controllerService.findControllerActive();
		if (controllerActive.size()>0) {
			for (DataController controller : controllerActive) {
				logger.info("controller active IP: "+controller.getIp());
				logger.info("username : "+controller.getUser());
				logger.info("Password : "+controller.getPassword());
				logger.info("status: "+stat);
				String respon = updateCard(controller.getUser(), controller.getPassword(),controller.getIp(),address,uid,stat);
				logger.info("RESPON : "+respon);
				logger.info("====== tunggu =======");
				Thread.sleep(500);
			}
			
		} else {
			logger.info("Nothing contoller active");
		}
		return new ResponseEntity<String>("OK",HttpStatus.OK);
	}
	
	public String updateCard(String username,String password,String ip, String address, String uid, String status) throws Exception,ParseException {
		String login = username+":"+password;
		String base64login = new String(Base64Utils.encode(login.getBytes()));
		String split [] = uid.split("\\:");
		try {
			
			Connection.Response response = Jsoup.connect("http://"+ip+"/UserParam.cgi")
            .timeout(10 * 1000)
            .header("Authorization", "Basic " + base64login)
            .method(Connection.Method.POST)
            .data("uAddr", address)
            .data("uName", "No")
            .data("uMode", status)
            .data("uPIN", "0")
            .data("uUIDsite", split[0])
            .data("uUIDcard", split[1])
            .data("cGate0", "")
            .data("uZone0", "0")
            .data("cGate1", "")
            .data("uZone1", "0")
            .data("uLevel", "0")
            .data("cDateE", "CHECKED")
            .data("uDayBegin", "00-01-01")
            .data("uDayEnd", "99-01-01")
            .followRedirects(true)
            .execute();
			
			Document document = response.parse();	
//			Map<String, String> mapCookies = response.cookies();
//			System.out.println(document);
//			System.out.println("mapCokies:" +mapCookies);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info("Exception : "+e.getMessage());
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return "SUCCESS";
	}
}
