package com.hanifalbaaits.java.model;

import org.springframework.stereotype.Component;

import com.hanifalbaaits.java.model.ResponModel;

@Component
public class ResponAPI {
	
	public ResponModel OK_200(String message) {
		//	OK(200, "OK")
		ResponModel model = new ResponModel();
		model.setMessage(message);
		model.setStatus(200);
		return model;
	}
	
	public ResponModel CREATED_201(String message) {
		//	CREATED(201, "Created")
		ResponModel model = new ResponModel();
		model.setMessage(message);
		model.setStatus(201);
		return model;
	}
	
	public ResponModel ACCEPT_202(String message) {
		//	ACCEPTED(202, "Accepted")
		ResponModel model = new ResponModel();
		model.setMessage(message);
		model.setStatus(202);
		return model;
	}
	
	public ResponModel BAD_REQUEST_400(String message) {
		//BAD_REQUEST(400, "Bad Request")
		ResponModel model = new ResponModel();
		model.setMessage(message);
		model.setStatus(400);
		return model;
	}
	
	public ResponModel UNAUTHORIZED_401(String message) {
		//UNAUTHORIZED(401, "Unauthorized")
		ResponModel model = new ResponModel();
		model.setMessage(message);
		model.setStatus(401);
		return model;
	}
	
	public ResponModel NOT_FOUND_404(String message) {
		//NOT_FOUND(404, message)
		ResponModel model = new ResponModel();
		model.setMessage(message);
		model.setStatus(404);
		return model;
	}
}
