package com.hanifalbaaits.java.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_card")
public class DataController {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="ip")
	private String ip;
	
	@Column(name="user")
	private String user;
	
	@Column(name="password")
	private String password;
	
	@Column(name="status")
	private int status;
	
	@Column(name="name_card")
	private String name_card;
	
	@Column(name="netmask")
	private String netmask;
	
	@Column(name="dgw")
	private String dgw;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getName_card() {
		return name_card;
	}

	public void setName_card(String name_card) {
		this.name_card = name_card;
	}

	public String getNetmask() {
		return netmask;
	}

	public void setNetmask(String netmask) {
		this.netmask = netmask;
	}

	public String getDgw() {
		return dgw;
	}

	public void setDgw(String dgw) {
		this.dgw = dgw;
	}
}
