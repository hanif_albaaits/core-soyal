package com.hanifalbaaits.java.model;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_data")
public class DataTap {

	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="data")
	private String data;
	
	@Column(name="date")
	private String date;
	
	@Column(name="time")
	private Time time;
	
	@Column(name="address")
	private String address;
	
	@Column(name="display")
	private String display;
	
	@Column(name="access_detail")
	private String detail;
	
	@Column(name="card_uid")
	private String uid;
	
	@Column(name="door")
	private int door;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public int getDoor() {
		return door;
	}

	public void setDoor(int door) {
		this.door = door;
	}
}
