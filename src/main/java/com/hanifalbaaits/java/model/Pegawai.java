package com.hanifalbaaits.java.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_pegawai")
public class Pegawai {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="nik")
	private String nik;
	
	@Column(name="nama_pegawai")
	private String nama;
	
	@Column(name="tpt_lahir")
	private String tlahir;
	
	@Column(name="tgl_lahir")
	private Date tglahir;
	
	@Column(name="jk")
	private String jk;
	
	@Column(name="email")
	private String email;
	
	@Column(name="noHP")
	private String noHp;
	
	@Column(name="id_dir")
	private int id_dir;
	
	@Column(name="id_dep")
	private int id_dep;
	
	@Column(name="id_div")
	private int id_div;
	
	@Column(name="id_jabatan")
	private int id_jabatan;
	
	@Column(name="id_status")
	private int id_status;
	
	@Column(name="UID")
	private String uid;
	
	@Column(name="address_uid")
	private String address_uid;
	
	@Column(name="status")
	private int status;

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTlahir() {
		return tlahir;
	}

	public void setTlahir(String tlahir) {
		this.tlahir = tlahir;
	}

	public Date getTglahir() {
		return tglahir;
	}

	public void setTglahir(Date tglahir) {
		this.tglahir = tglahir;
	}

	public String getJk() {
		return jk;
	}

	public void setJk(String jk) {
		this.jk = jk;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNoHp() {
		return noHp;
	}

	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}

	public int getId_dir() {
		return id_dir;
	}

	public void setId_dir(int id_dir) {
		this.id_dir = id_dir;
	}

	public int getId_dep() {
		return id_dep;
	}

	public void setId_dep(int id_dep) {
		this.id_dep = id_dep;
	}

	public int getId_div() {
		return id_div;
	}

	public void setId_div(int id_div) {
		this.id_div = id_div;
	}

	public int getId_jabatan() {
		return id_jabatan;
	}

	public void setId_jabatan(int id_jabatan) {
		this.id_jabatan = id_jabatan;
	}

	public int getId_status() {
		return id_status;
	}

	public void setId_status(int id_status) {
		this.id_status = id_status;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getAddress_uid() {
		return address_uid;
	}

	public void setAddress_uid(String address_uid) {
		this.address_uid = address_uid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
